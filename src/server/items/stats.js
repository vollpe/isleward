define([

], function (

) {
	return {
		names: {
			armor: 'Armor',
			maxHp: 'HP',
			mana: 'Mana',
			regenHp: 'HP Regeneration',
			regenMana: 'Mana Regeneration',
			str: 'Strength',
			int: 'Intellect',
			dex: 'Dexterity',
			critChance: 'Global Critical Chance',
			critMultiplier: 'Global Critical Multiplier',
			attackCritChance: 'Attack Critical Chance',
			attackCritMultiplier: 'Attack Critical Multiplier',
			spellCritChance: 'Spell Critical Chance',
			spellCritMultiplier: 'Spell Critical Multiplier',
			attackSpeed: 'Attack Speed',
			sprintChance: 'Sprint Chance',
			attackRange: 'Attack Range',
			luck: 'Luck'
		}
	};
});
